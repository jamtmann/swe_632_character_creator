// Wait for the DOM to be ready
//https://www.sitepoint.com/basic-jquery-form-validation-tutorial/
$(function() {
  // Initialize form validation on the stats form.
  // It has the name attribute "stats"
  $("form[name='stats']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name stats
      // of an input field. Validation rules are defined
      // on the right side
      strBox: {
        required: true,
        digits: true,
        range: [8,17]
      },
      dexBox: {
        required: true,
        digits: true,
        range: [8,17]
      },
      conBox: {
        required: true,
        digits: true,
        range: [8,17]
      },
      intBox: {
        required: true,
        digits: true,
        range: [8,17]
      },
      wisBox: {
        required: true,
        digits: true,
        range: [8,17]
      },
      chaBox: {
        required: true,
        digits: true,
        range: [8,17]
      },

    },
    // Specify validation error messages
    messages: {
      strBox: {
        required: "Please provide your strength",
        range: "Your strength must be between 8 and 17",
        digits: "Your strength must be an int between 8 and 17"
      },     
      dexBox: {
        required: "Please provide your dexterity",
        range: "Your dexterity must be between 8 and 17",
        digits: "Your dexterity must be an int between 8 and 17"
      },
      conBox: {
        required: "Please provide your constitution",
        range: "Your constitution must be between 8 and 17",
        digits: "Your constitution must be an int between 8 and 17"
      },
      intBox: {
        required: "Please provide your intelligence",
        range: "Your intelligence must be between 8 and 17",
        digits: "Your intelligence must be an int between 8 and 17"
      },
      wisBox: {
        required: "Please provide your wisdom",
        range: "Your wisdom must be between 8 and 17",
        digits: "Your wisdom must be an int between 8 and 17"
      },
      chaBox: {
        required: "Please provide your charisma",
        range: "Your charisma must be between 8 and 17",
        digits: "Your charisma must be an int between 8 and 17"
      },
    },
    /*
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
    */
  });
});
