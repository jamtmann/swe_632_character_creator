var level;
var charClass;
var race;
var stats;
var totalStats;
var hitpointRollType;
var hitdie;
var hitpoints;
var hitpointRolls;
var proficiencyBonus;
var saves;
var attributeFields = ["strBox","dexBox","conBox","intBox","wisBox","chaBox"];
var drawFlag

window.onload = function() {
	initialize();
	
	//add listeners to interactable elements
	document.getElementById("levelup").addEventListener("click", levelup);
	document.getElementById("leveldown").addEventListener("click", leveldown);
	document.getElementById("raceSelect").addEventListener("change", changeRace);
	document.getElementById("classSelect").addEventListener("change", changeClass);
	
	document.getElementById("average").checked = true;
	document.getElementById("average").addEventListener("change", hitpointChange);
	document.getElementById("maximum").addEventListener("change", hitpointChange);
	document.getElementById("random").addEventListener("change", hitpointChange);
	
	document.getElementById("randomizer").addEventListener("click", function(){clearCanvas(false);randomize();});
	//document.getElementById("drawerBox").addEventListener("click", drawCharacter);
	document.getElementById("reset").addEventListener("click", function(){clearCanvas(true);clearErrors();initialize();resetFeedbackImages();});
	clearCanvas(true);
	
	var formElements = document.getElementById("statsForm").elements;
	for (var i = 0; i < formElements.length; i++)
		if (formElements[i].nodeName == "INPUT") {
			formElements[i].addEventListener("change", function(event){statChange(event);if (drawFlag)drawCharacter();});
			formElements[i].value = "";
		}
}

function statChangeAndDraw(event) {
	statChange(event);
	drawCharacter();
}

function clearErrors(){
    var validator = $( "#statsForm" ).validate();
            validator.resetForm();
}
function randomize() {
    clearErrors();
	//need to call draw only once here but still need to draw on individual stat edits
	//could make updateStats function/rig it manually + remove draw from change event
	drawFlag = false;
	for (var i = 0; i < attributeFields.length; i++) {
		var current = document.getElementById(attributeFields[i]);
		current.value = Math.floor(Math.random() * 10 + 8);
		var event = new Event("change");
		current.dispatchEvent(event);
	}
	drawFlag = true;
    drawCharacter();
}

function initialize() {
	level = 0;
	hitpoints = 0;
	hitpointRollType = "average";
	hitpointRolls = [];
	stats = [8,8,8,8,8,8];
	totalStats = [8,8,8,8,8,8];
	saves = [0,0,0,0,0,0];
	document.getElementById("raceSelect").value = "human";
	document.getElementById("classSelect").value = "fighter";
	changeRace();
	changeClass();
	levelup();
	drawFlag = true;
}

function statChange(event) {
	var index = 0;
	if (event.target.id == "strBox")
		index = 0;
	if (event.target.id == "dexBox")
		index = 1;
	if (event.target.id == "conBox")
		index = 2;
	if (event.target.id == "intBox")
		index = 3;
	if (event.target.id == "wisBox")
		index = 4;
	if (event.target.id == "chaBox")
		index = 5;
	
	updateFeedbackImage(event.target.value, index);
	if (event.target.value != "")
		stats[index] = parseInt(event.target.value);
	else
		stats[index] = 8;
	//this also determines saves
	determineTotalStats();
	if (index == 2)
		updateHitpoints();
}

function updateFeedbackImage(input, index) {
	var feedbackImages = document.getElementsByClassName("feedback");
	if (!isNaN(input) && parseInt(input) == parseFloat(input) &&
			input.indexOf(".") == -1 && parseInt(input) > 7 &&
			parseInt(input) < 18) {
		feedbackImages[index].style.backgroundImage = "url('img/check.png')";
	} else if (input != "") {
		feedbackImages[index].style.backgroundImage = "url('img/cross.png')";
	} else {
		feedbackImages[index].style.backgroundImage = "url('img/dash.png')";
	}
}

function resetFeedbackImages() {
	var feedbackImages = document.getElementsByClassName("feedback");
	for (var i = 0; i < feedbackImages.length; i++) {
		feedbackImages[i].style.backgroundImage = "url('img/dash.png')";
	}
}

function levelup() {
	if (level < 20) {
		level++;
		addHitpoints();
		determineProficiency();
		determineSaves();
		document.getElementById("level").innerHTML = level;
		checkLevelButtons();
	}
}

function leveldown() {
	if (level > 1) {
		level--;
		removeHitpoints();
		determineProficiency();
		determineSaves();
		document.getElementById("level").innerHTML = level;
		checkLevelButtons();
	}
}

function checkLevelButtons() {
	if (level < 2) {
		document.getElementById("leveldown").setAttribute("disabled", "disabled");
		document.getElementById("levelup").removeAttribute("disabled");
	} else if (level > 19) {
		document.getElementById("levelup").setAttribute("disabled", "disabled");
		document.getElementById("leveldown").removeAttribute("disabled");
	} else {
		document.getElementById("leveldown").removeAttribute("disabled");
		document.getElementById("levelup").removeAttribute("disabled");
	}
}

//sets hitpoints to the sum of hitPointRolls and updates UI element
function updateHitpoints() {
	hitpoints = hitpointRolls.reduce(function(a, b){
        return a + b;
    }, 0);
	hitpoints += Math.floor((totalStats[2] - 10) / 2) * level;
	document.getElementById("hp").innerHTML = hitpoints;
}

//adds hitpoints according to hitpointRollType
//called by levelup and rollTotalHitpoints
function addHitpoints() {
	if (hitpointRollType == "average")
		hitpointRolls.push(Math.ceil((hitdie + 1) / 2));
	if (hitpointRollType == "maximum")
		hitpointRolls.push(hitdie);
	if (hitpointRollType == "random")
		hitpointRolls.push(Math.floor(Math.random() * hitdie) + 1);
	
	updateHitpoints();
}

function removeHitpoints() {
	hitpointRolls.pop();
	updateHitpoints();
}

//rolls hitpoints for every level
//called on class change or hitpoint roll type change
function rollTotalHitpoints() {
	hitpointRolls = [];
	for (var i = 0; i < level; i++)
		addHitpoints();
}

function hitpointChange() {
	if (document.getElementById("average").checked)
		hitpointRollType = "average";
	if (document.getElementById("maximum").checked)
		hitpointRollType = "maximum";
	if (document.getElementById("random").checked)
		hitpointRollType = "random";
	
	rollTotalHitpoints();
}

function changeRace() {
	race = document.getElementById("raceSelect").value;
	determineTotalStats();
	document.getElementById("race").innerHTML = race.charAt(0).toUpperCase() + race.slice(1);
}

function changeClass() {
	charClass = document.getElementById("classSelect").value;
	
	if (charClass == "fighter")
		hitdie = 10;
	if (charClass == "wizard")
		hitdie = 6;
	if (charClass == "rogue")
		hitdie = 8;
	if (charClass == "cleric")
		hitdie = 8;
	if (charClass == "ranger")
		hitdie = 10;
	
	rollTotalHitpoints();
	determineSaves();
	document.getElementById("hitdie").innerHTML = "1-" + hitdie;
	document.getElementById("class").innerHTML = charClass.charAt(0).toUpperCase() + charClass.slice(1);
}

function determineTotalStats() {
	for (var i = 0; i < 6; i++)
		totalStats[i] = stats[i];
	
	if (race == "dwarf")
		totalStats[2] += 2;
	if (race == "halfling")
		totalStats[1] += 2;
	if (race == "elf")
		totalStats[1] += 2;
	if (race == "orc") {
		totalStats[0] += 2;
		totalStats[2] += 1;
	}
	if (race == "human")
		for (var i = 0; i < 6; i++)
			totalStats[i]++;
	
	statsListItems = document.getElementById("totalStats").children;
	statsListItems[0].innerHTML = "Strength: " + totalStats[0];
	statsListItems[1].innerHTML = "Dexterity: " + totalStats[1];
	statsListItems[2].innerHTML = "Constitution: " + totalStats[2];
	statsListItems[3].innerHTML = "Intelligence: " + totalStats[3];
	statsListItems[4].innerHTML = "Wisdom: " + totalStats[4];
	statsListItems[5].innerHTML = "Charisma: " + totalStats[5];
	
	determineSaves();
}

function determineSaves() {
	for (var i = 0; i < 6; i++)
		saves[i] = Math.floor((totalStats[i] - 10) / 2);
	
	if (charClass == "fighter") {
		saves[0] += proficiencyBonus;
		saves[2] += proficiencyBonus;
	} else if (charClass == "wizard") {
		saves[3] += proficiencyBonus;
		saves[4] += proficiencyBonus;
	} else if (charClass == "rogue") {
		saves[1] += proficiencyBonus;
		saves[3] += proficiencyBonus;
	} else if (charClass == "cleric") {
		saves[5] += proficiencyBonus;
		saves[6] += proficiencyBonus;
	} else if (charClass == "ranger") {
		saves[0] += proficiencyBonus;
		saves[1] += proficiencyBonus;
	}
	
	savesListItems = document.getElementById("saves").children;
	savesListItems[0].innerHTML = "Strength: " + saves[0];
	savesListItems[1].innerHTML = "Dexterity: " + saves[1];
	savesListItems[2].innerHTML = "Constitution: " + saves[2];
	savesListItems[3].innerHTML = "Intelligence: " + saves[3];
	savesListItems[4].innerHTML = "Wisdom: " + saves[4];
	savesListItems[5].innerHTML = "Charisma: " + saves[5];
}

function determineProficiency() {
	if (level < 5)
		proficiencyBonus = 2;
	else if (level < 9)
		proficiencyBonus = 3;
	else if (level < 13)
		proficiencyBonus = 4;
	else if (level < 17)
		proficiencyBonus = 5;
	else
		proficiencyBonus = 6;
	
	document.getElementById("proficiency").innerHTML = proficiencyBonus;
}



function test() {
	var c = document.getElementById("canvas");
	var ctx = c.getContext("2d");
	var img = document.getElementById("testImg");
	ctx.drawImage(img,10,10);
	ctx.drawImage(img,20,10);
}

function clearCanvas(drawPlaceholder) {
	var c = document.getElementById("canvas");
	var ctx = c.getContext("2d");
	var img = document.getElementById("testImg");
    ctx.clearRect(0, 0, c.width, c.height);
	if (drawPlaceholder) {
		draw("elf/silhouette.png");
	}
}

function draw(target) {
	var c = document.getElementById("canvas");
	var ctx = c.getContext("2d");
	//var img = document.getElementById("target");
	var img = new Image;
    img.src = target;
    img.onload = function(){
        ctx.drawImage(img,0,0);
    };
	
}

function getArms(stats0){
    //02_arms
	//STR stat
    var target = "";
    if (stats0 < 8)
       target = "elf/00_blank.png";
	else if (stats0 < 10)
	   target = "elf/02_arms/arm_8_str.png";
	else if (stats0 < 12)
	   target = "elf/02_arms/arm_10_str.png";
	else if (stats0 < 14)
	   target = "elf/02_arms/arm_12_str.png";
	else if (stats0 < 16)
	   target = "elf/02_arms/arm_14_str.png";
	else if (stats0 < 18)
	   target = "elf/02_arms/arm_16_str.png";
    else
       target = "elf/00_blank.png";
    return target;
}

function getBody(stats5){
	//03_body
	//cha
    var target = "";
    if (stats5 < 8)
       target = "elf/00_blank.png";
	else if (stats5 < 10)
	   target = "elf/03_body/body_8_cha.png";
	else if (stats5 < 12)
	   target = "elf/03_body/body_10_cha.png";
	else if (stats5 < 14)
	   target = "elf/03_body/body_12_cha.png";
	else if (stats5 < 16)
	   target = "elf/03_body/body_14_cha.png";
	else if (stats5 < 18)
	   target = "elf/03_body/body_16_cha.png";
   else
       target = "elf/00_blank.png";
    return target;
}


function getHead(stats3){
    //07_head
	//INT stat
    var target = "";
    if (stats3 < 8)
       target = "elf/00_blank.png";
	else if (stats3 < 10)
		target = "elf/07_head/head_8_int.png";
	else if (stats3 < 12)
	   target = "elf/07_head/head_10_int.png";
	else if (stats3 < 14)
	   target = "elf/07_head/head_12_int.png";
	else if (stats3 < 16)
	   target = "elf/07_head/head_14_int.png";
	else if (stats3 < 18)
	   target = "elf/07_head/head_16_int.png";
    else
        target = "elf/00_blank.png";
    return target;
}


function getShoes(stats3, stats1){
    //01_shoes
	//int
    var target = "";
	if (stats3 < 14) {
		//dex
        if (stats1 < 8)
           target = "elf/00_blank.png";
		else if (stats1 < 10)
		   target = "elf/01_shoes/plate_shoes.png";
		else if (stats1 < 12)
		   target = "elf/01_shoes/plate_shoes.png";
		else if (stats1 < 14)
		   target = "elf/01_shoes/cloth_shoes.png";
		else if (stats1 < 16)
		   target = "elf/01_shoes/leather_shoes.png";
		else if (stats1 < 18)
		   target = "elf/01_shoes/leather_decorated_shoes.png";
        else
           target = "elf/00_blank.png";
	} else {
		//dex
        if (stats1 < 8)
           target = "elf/00_blank.png";
		else if (stats1 < 10)
		   target = "elf/01_shoes/plate_shoes.png";
		else if (stats1 < 12)
		   target = "elf/01_shoes/leather_shoes.png";
		else if (stats1 < 14)
		   target = "elf/01_shoes/leather_shoes.png";
		else if (stats1 < 16)
		   target = "elf/01_shoes/cloth_shoes.png";
		else if (stats1 < 18)
		   target = "elf/01_shoes/cloth_decorated_shoes.png";
        else
           target = "elf/00_blank.png";
	}
    return target;
}

function getLegs(stats0, stats5){
//04_leg_decorations
	//str
    var target = "";
	if (stats0 >= 14) {
		//CHA stat
		if (stats5 < 14)
			target = "elf/04_leg_decorations/leg_plates.png";
		else if (stats5 < 18)
			target = "elf/04_leg_decorations/leg_plates_2.png";
        else
        target = "elf/00_blank.png";
	} else {
		target = "elf/00_blank.png";
	}
    return target;
}

function getBelt(stats2){
	//05_belts
	//con
    var target = "";
	if (stats2 < 10)
	   target = "elf/00_blank.png";
	else if (stats2 < 12)
	   target = "elf/05_belts/belt_10_con.png";
	else if (stats2 < 14)
	   target = "elf/05_belts/belt_12_con.png";
	else if (stats2 < 16)
	   target = "elf/05_belts/belt_14_con.png";
	else if (stats2 <18)
	   target = "elf/05_belts/belt_16_con.png";
    else
       target = "elf/00_blank.png";
    return target;
}


function getChest(stats5){
//06_chest_decorations
	//cha
    var target = "";
	if (stats5 < 14){
		target = "elf/00_blank.png";
	}else if(stats5 < 18){
		target = "elf/06_chest_decorations/tie.png";
    }else{
        target = "elf/00_blank.png";
    }
    return target;
}	


function getGloves(stats3, stats1){
	//08_gloves
	//int
    var target = "";
	if (stats3 < 14) {
		//dex
        if (stats1 < 8)
           target = "elf/00_blank.png";
		else if (stats1 < 10)
		   target = "elf/08_gloves/plate_gloves.png";
		else if (stats1 < 12)
		   target = "elf/08_gloves/plate_gloves.png";
		else if (stats1 < 14)
		   target = "elf/08_gloves/cloth_gloves.png";
		else if (stats1 < 16)
		   target = "elf/08_gloves/leather_gloves.png";
		else if (stats1 < 18)
		   target = "elf/08_gloves/leather_gloves.png";
        else
            target = "elf/00_blank.png";
	} else {
		//dex
		if (stats1 < 8)
           target = "elf/00_blank.png";
		else if (stats1 < 10)
		   target = "elf/08_gloves/plate_gloves.png";
		else if (stats1 < 12)
		   target = "elf/08_gloves/leather_gloves.png";
		else if (stats1 < 14)
		   target = "elf/08_gloves/leather_gloves.png";
		else if (stats1 < 16)
		   target = "elf/08_gloves/cloth_gloves.png";
        else if (stats1 < 18)
		   target = "elf/08_gloves/cloth_gloves.png";
       else
            target = "elf/00_blank.png";
	}
    return target;
}   

function getWis(stats4){
	//09_wisdom
	//wis
    var target = "";
	if (stats4 < 10)
	   target = "elf/00_blank.png";
	else if (stats4 < 12)
	   target = "elf/09_wisdom/foreground_10_wis.png";
	else if (stats4 < 14)
	   target = "elf/09_wisdom/foreground_12_wis.png";
	else if (stats4 < 16)
	   target = "elf/09_wisdom/foreground_14_wis.png";
	else if (stats4 < 18)
	   target = "elf/09_wisdom/foreground_16_wis.png";
    return target;
}  
/*
function isValidInput(input) {
	if (isNaN(input) || in)
}
*/
function drawCharacter() {
    clearCanvas(false);
	var form = document.getElementById("statsForm").elements;
	var stats = [0,0,0,0,0,0];
	for (var i = 0; i < stats.length; i++) {
		stats[i] = form[i].value;
        if(isNaN(form[i].value) ||
				form[i].value.indexOf(".") != -1){
            clearCanvas(true)
			return;
        }
	}
	
    if((stats[0] < 8) ||
    (stats[1] < 8) ||
    (stats[2] < 8) ||
    (stats[3] < 8) ||
    (stats[4] < 8) ||
    (stats[5] < 8)){
		clearCanvas(true);
        return;
    }
    if((stats[0] > 17) ||
    (stats[1] > 17) ||
    (stats[2] > 17) ||
    (stats[3] > 17) ||
    (stats[4] > 17) ||
    (stats[5] > 17)){
		clearCanvas(true);
        return;
    }
    
    // Create the loader and queue our 3 images. Images will not 
    // begin downloading until we tell the loader to start. 
    var loader = new PxLoader(), 
        glovesImg = loader.addImage(getGloves(stats[3], stats[1])),
        
        headImg = loader.addImage(getHead(stats[3])), 
        shoesImg = loader.addImage(getShoes(stats[3], stats[1])), 
        bodyImg = loader.addImage(getBody(stats[5])), 
        beltImg = loader.addImage(getBelt(stats[2])), 
        
        legsImg = loader.addImage(getLegs(stats[0], stats[5])), 
        chestImg = loader.addImage(getChest(stats[5])), 
        armsImg = loader.addImage(getArms(stats[0])); 
        wisImg = loader.addImage(getWis(stats[4]));
        
    // callback that will be run once images are ready 
    loader.addCompletionListener(function() { 
        var canvas = document.getElementById("canvas"), 
            ctx = canvas.getContext("2d"); 
        
        ctx.drawImage(shoesImg, 0, 0); 
        ctx.drawImage(headImg, 0, 0); 
        ctx.drawImage(armsImg, 0, 0); 
        ctx.drawImage(bodyImg, 0, 0); 
        ctx.drawImage(legsImg, 0, 0); 
        ctx.drawImage(beltImg, 0, 0); 
        ctx.drawImage(chestImg, 0, 0); 
        ctx.drawImage(glovesImg, 0, 0); 
        ctx.drawImage(wisImg, 0, 0); 
    }); 
     
    // begin downloading images 
    loader.start(); 
}